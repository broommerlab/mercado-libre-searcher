//
//  MercadoLibreSearcherTests.swift
//  MercadoLibreSearcherTests
//
//  Created by Miller Mosquera on 7/09/21.
//

import XCTest
@testable import MercadoLibreSearcher

class MercadoLibreSearcherTests: XCTestCase {

	var usecase: iSearchProductUseCase!
	
    override func setUpWithError() throws {
		super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
		usecase = SearchProductsUseCase()
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
		usecase = nil
		super.tearDown()
    }

    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
	
	func test_search_did_return_empty_list() throws {
		
		let expectation = XCTestExpectation(description: "Downloading list")
		
		let request = ""
		
		var valueExpected: Int!
		usecase.execute(request: request) { data in
			valueExpected = data!.results.count
			XCTAssertEqual(valueExpected, 0)
			expectation.fulfill()
		} errorHandler: { error in
			print("not today")
		}
		
		wait(for: [expectation], timeout: 5.0)
	}
	
	func test_search_did_return_list() throws {
		let expectation = XCTestExpectation(description: "Downloading list")
		
		let request = "iphone"
		
		var valueExpected: Int!
		
		usecase.execute(request: request) { data in
			valueExpected = data!.results.count
			XCTAssertGreaterThan(valueExpected, 0)
			expectation.fulfill()
		} errorHandler: { error in
			print("not today")
		}

		wait(for: [expectation], timeout: 5.0)
	}
	
	func test_search_did_show_up_error() throws {
		let expectation = XCTestExpectation(description: "Downloading error")
		
		let request = "/"
		usecase.execute(request: request) { data in
			print("not today")
		} errorHandler: { error in
			XCTAssertEqual(error, .invalidResponse)
			expectation.fulfill()
		}
		
		wait(for: [expectation], timeout: 5.0)
	}
	
	func searchDidReturnNotFound() throws {
		
	}

}
