//
//  ServiceLocator.swift
//  MercadoLibreSearcher
//
//  Created by Miller Mosquera on 8/09/21.
//

class ServiceLocator {
	
	static let instance = ServiceLocator()
	private lazy var services: [String: Any] = [:]

	private func typeName(obj: Any) -> String {
		return (obj is Any.Type) ? "\(obj)" : "\(type(of: obj))"
	}
	
	func get<T>() -> T?{
		let key = "\(T.self)"
		print(services)
		return services[key] as? T
	}
	
	private func add<T>(service: T) {
		let key = typeName(obj: T.self)
		services[key] = service as Any
	}
	
	func loadModules(){
		#if MOCK
		add(service: FetchProductsRepositoryMock() as iFetchProductsRepository)
		#else
		add(service: FetchProductsRepository() as iFetchProductsRepository)
		#endif
	}
}
