//
//  FetchProductsRepository.swift
//  MercadoLibreSearcher
//
//  Created by Miller Mosquera on 9/09/21.
//
import SwiftyJSON
import Foundation

protocol iFetchProductsRepository {
	func fetchProducts(by text: String,
					   response: @escaping( _ data: Results) -> (),
					   errorHndr: @escaping( _ errorDescription: NetworkError) -> ())
}

class FetchProductsRepository: BaseRepository, iFetchProductsRepository {
	
	private let URLEndPoint = "MLA/search?"
	private var analyzeResult: AnalyzeAndMatchResult = MatchProducts()
	
	func fetchProducts(by text: String, response: @escaping (Results) -> (), errorHndr: @escaping (NetworkError) -> ()) {
		requestParams = ["q": text]
		headerParams = [.accept("application/json")]
		fullUrl = urlHelper.buildUrl(baseURL: BaseURL, endpoint: URLEndPoint)
		
		consumeService(method: .get) { [self] data in
			if let matchedData = analyzeResult.matchData(json: data) as? Results {
				response(matchedData)
			}
		} errorCompletion: { error in
			print("error: \(error.localizedDescription)")
			errorHndr(error)
		}
	}
}
