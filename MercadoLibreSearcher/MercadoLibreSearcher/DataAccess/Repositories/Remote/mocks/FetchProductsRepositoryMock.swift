//
//  FetchProductsRepositoryMock.swift
//  MercadoLibreSearcher
//
//  Created by Miller Mosquera on 10/09/21.
//
import Foundation
class FetchProductsRepositoryMock: iFetchProductsRepository {

	var results:Results!
	var empty = ""
	var error = "/"
	
	func fetchProducts(by text: String, response: @escaping (Results) -> (), errorHndr: @escaping (NetworkError) -> ()) {
		switch text {
		case empty:
			response(emptyCase())
			break
		case error:
			errorHndr(.invalidResponse)
			break
		default:
			response(fillListCase(text: text))
		}
	}
	
	private func emptyCase() -> Results{
		let products = [Product]()
		results = Results(site_id: "some", query: "", results: products)
		return results
	}
	
	private func fillListCase(text: String) -> Results{
		
		var products = [Product]()
		products.append(Product(id: "1", title: "Sample product 1", thumbnail: "", price: "$11.900"))
		products.append(Product(id: "2", title: "Sample product 1", thumbnail: "", price: "$10.900"))
		products.append(Product(id: "3", title: "Sample product 1", thumbnail: "", price: "$9.500"))
		results = Results(site_id: "some", query: text, results: products)
		return results
	}
	
}
