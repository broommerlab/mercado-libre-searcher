//
//  String+Extension.swift
//  MercadoLibreSearcher
//
//  Created by Miller Mosquera on 12/09/21.
//

import Foundation

extension String{
	
	var localized: String{
		NSLocalizedString(self, comment: " ")
	}
	
	func currencyFormat(value: Double) -> String{
		var stringValue = self
		let formatter = NumberFormatter()
		formatter.locale = Locale.current
		formatter.numberStyle = .currency
		if let formattedPrice = formatter.string(from: value as NSNumber) {
			stringValue = formattedPrice
			return stringValue
		}
		return stringValue
	}
}
