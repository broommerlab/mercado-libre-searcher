//
//  ProductsSourceDelegate.swift
//  MercadoLibreSearcher
//
//  Created by Miller Mosquera on 13/09/21.
//

protocol ProductsSourceDelegate {
	func itemSelected(item: Product)
}
