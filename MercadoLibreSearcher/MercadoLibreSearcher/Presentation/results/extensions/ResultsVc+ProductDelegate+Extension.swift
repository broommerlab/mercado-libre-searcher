//
//  ResultsVc+ProductDelegate+Extension.swift
//  MercadoLibreSearcher
//
//  Created by Miller Mosquera on 13/09/21.
//

import Foundation
extension ResultsVc: ProductsSourceDelegate{
	func itemSelected(item: Product) {
		coordinator?.showDetails(product: item)
	}
}
