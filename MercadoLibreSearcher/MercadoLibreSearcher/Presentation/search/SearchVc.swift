//
//  SearchVc.swift
//  MercadoLibreSearcher
//
//  Created by Miller Mosquera on 7/09/21.
//

import UIKit

class SearchVc: BaseViewController {
	
	var coordinator: SearchCoordinator?
	
	private lazy var customSearchBar: CustomSearchBar = {
		let searchBar = CustomSearchBar()
		searchBar.placeholder = Strings.searcher_title.localized
		searchBar.setupSearchBar(parent: self, resultsVc: RecentlySearchedVc())
		searchBar.delegate?.actionsDelegate = self
		return searchBar
	}()
	
	private let searchView: SearchScreenView = {
		let view = SearchScreenView(frame: .zero)
		view.translatesAutoresizingMaskIntoConstraints = false
		return view
	}()
	
    override func viewDidLoad() {
        super.viewDidLoad()
		setupViews()
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		navigationController?.setNavigationBarHidden(true, animated: animated)
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		navigationController?.setNavigationBarHidden(true, animated: animated)
	}
	
	func setupViews(){
		searchView.bindConstraints(to: self.view)
		searchView.setupSearchBarView(searchBar: customSearchBar.getSearchBar())
	}
}

extension SearchVc: SearchBarActionsDelegate{
	func searchButtonTapped(text: String) {
		coordinator?.showResultSearch(text: text)
	}
	
	func searchBarTapped() {
		//searchView.searchBarToTop()
	}
	
	func searchBarWasClosed() {
		//searchView.searchBarToStartPosition()
	}
}
