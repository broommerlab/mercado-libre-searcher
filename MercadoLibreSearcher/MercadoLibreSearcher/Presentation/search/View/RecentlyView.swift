//
//  RecentlyView.swift
//  MercadoLibreSearcher
//
//  Created by Miller Mosquera on 13/09/21.
//

import UIKit
class RecentlyView: UIView{
	
	private let LOGO_SIZE_HEIGHT: CGFloat = 128
	private let LOGO_SIZE_WIDTH: CGFloat = 167
	private var imageHelper: ImageHelper!
	private var colorHelper: ColorHelper!
	
	private lazy var logoImg: UIImageView = {
		let img = UIImageView(frame: .zero)
		img.image = imageHelper.showImage(named: "logo")
		img.translatesAutoresizingMaskIntoConstraints = false
		return img
	}()
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		imageHelper = ImageHelper()
		colorHelper = ColorHelper()
		setupViews()
		setConstraints()
	}
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	func setupViews() {
		backgroundColor = colorHelper.getColor(color: .primary)
		addSubview(logoImg)
	}
	
	func setConstraints() {
		let safeArea = self.safeAreaLayoutGuide

		NSLayoutConstraint.activate([
			logoImg.centerXAnchor.constraint(equalTo: safeArea.centerXAnchor),
			logoImg.heightAnchor.constraint(equalToConstant: LOGO_SIZE_HEIGHT),
			logoImg.widthAnchor.constraint(equalToConstant: LOGO_SIZE_WIDTH)
		])
	}
	
	func bindConstraints(to parentView: UIView){
		parentView.addSubview(self)
		NSLayoutConstraint.activate([
			self.topAnchor.constraint(equalTo: parentView.topAnchor, constant: 200),
			self.leftAnchor.constraint(equalTo: parentView.leftAnchor),
			self.rightAnchor.constraint(equalTo: parentView.rightAnchor),
			self.bottomAnchor.constraint(equalTo: parentView.bottomAnchor)
		])
	}
	
}
