//
//  SearchProductsViewModel.swift
//  MercadoLibreSearcher
//
//  Created by Miller Mosquera on 7/09/21.
//
import Foundation

class SearchProductsViewModel: BaseViewModel<String, Results> {
	
	private let usecase: iSearchProductUseCase = SearchProductsUseCase()
	
	override func initObserver() {
		usecase
			.execute(request: request!,
					 responseHandler: responseHandler(data:),
					 errorHandler: errorHandler(error:))
	}
	
	override func responseHandler(data: Results?) {
		if let data = data {
			send(data: data)
		}
	}
	
	override func errorHandler(error: NetworkError) {
		send(error: error)
	}
}
