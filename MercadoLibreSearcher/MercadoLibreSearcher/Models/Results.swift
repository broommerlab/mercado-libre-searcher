//
//  Results.swift
//  MercadoLibreSearcher
//
//  Created by Miller Mosquera on 10/09/21.
//

struct Results: Decodable {
	var site_id: String
	var query: String
	var results: [Product]
}
