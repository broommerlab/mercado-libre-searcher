//
//  ErrorView.swift
//  MercadoLibreSearcher
//
//  Created by Miller Mosquera on 13/09/21.
//

import UIKit
class ErrorView: UIView {
	
	private var messageLbl: UILabel = {
		let lbl = UILabel(frame: .zero)
		lbl.text = ""
		lbl.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
		lbl.textAlignment = .center
		lbl.numberOfLines = 1
		lbl.font = UIFont(name: "AvenirNext-regular", size: 14.0)
		lbl.translatesAutoresizingMaskIntoConstraints = false
		return lbl
	}()
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		setupViews()
		setConstraints()
	}
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	private func setupViews(){
		backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
		addSubview(messageLbl)
	}
	
	private func setConstraints(){
		let safeArea = self.safeAreaLayoutGuide

		NSLayoutConstraint.activate([
			messageLbl.topAnchor.constraint(equalTo: safeArea.topAnchor, constant: 10),
			messageLbl.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor, constant: 10),
			messageLbl.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor, constant: -10),
			messageLbl.bottomAnchor.constraint(equalTo: safeArea.bottomAnchor, constant: -10)
		])
	}
	
	func bindContraints(to parent: UIView){
		parent.addSubview(self)
		NSLayoutConstraint.activate([
			self.centerYAnchor.constraint(equalTo: parent.centerYAnchor),
			self.centerXAnchor.constraint(equalTo: parent.centerXAnchor),
		])
	}
	
	func setMessage(errorMessage: String){
		messageLbl.text = errorMessage
	}
}
