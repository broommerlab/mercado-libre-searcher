//
//  CustomSearchBarDelegate.swift
//  MercadoLibreSearcher
//
//  Created by Miller Mosquera on 9/09/21.
//
import UIKit

protocol SearchBarActionsDelegate {
	func searchButtonTapped(text: String)
	func searchBarTapped()
	func searchBarWasClosed()
}

class CustomSearchBarDelegate: NSObject, UISearchBarDelegate, UISearchResultsUpdating  {
	
	var actionsDelegate: SearchBarActionsDelegate? = nil
	
	func updateSearchResults(for searchController: UISearchController) {
		guard let text = searchController.searchBar.text else {return}
		print("from result new impl vc: \(text)")
	}
	
	func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
		guard let delegate = actionsDelegate else {return}
		delegate.searchBarWasClosed()
	}
	
	func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
		guard let delegate = actionsDelegate else {return}
		delegate.searchBarTapped()
	}
	
	func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
		guard let text = searchBar.text, let delegate = actionsDelegate else {return}
		print("Start searching: \(text)")
		delegate.searchButtonTapped(text: text)
	}
}
