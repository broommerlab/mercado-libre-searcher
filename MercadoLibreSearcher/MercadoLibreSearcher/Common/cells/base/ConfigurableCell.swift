//
//  ConfigurationCell.swift
//  MercadoLibreSearcher
//
//  Created by Miller Mosquera on 13/09/21.
//

import Foundation
import UIKit

protocol ConfigurableCell {
	associatedtype ItemType
	associatedtype CellType: UITableViewCell
	static func configureCell(indexPath: IndexPath, item: ItemType, cell: CellType)
	static func reuseIdentifierForIndexPath(indexPath: IndexPath) -> String
}
