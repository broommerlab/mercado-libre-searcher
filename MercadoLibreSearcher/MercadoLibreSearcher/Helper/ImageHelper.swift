//
//  ImageHelper.swift
//  MercadoLibreSearcher
//
//  Created by Miller Mosquera on 8/09/21.
//
import UIKit

class ImageHelper {
	func showImage(named: String) -> UIImage{
		return UIImage(named: named)!
	}
}
